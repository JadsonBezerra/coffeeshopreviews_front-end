import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class SingleCofeeShop extends Component{

    constructor(props){
        super(props);
        this.state={
            item:props.item
        }
    }  



    render(){
        return(
            
            <li className="collection-item">
            <Link to={`/CoffeeShopReviews/${this.state.item.id}`} style={{color:"#000"}}>
            <h5>
            {this.state.item.name}    <i className="fa fa-coffee"></i>
            </h5>
            in {this.state.item.city}
            </Link>
            </li>
        )
    }
}

export default SingleCofeeShop;