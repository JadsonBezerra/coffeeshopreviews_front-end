import React, {Component} from 'react';
import axios from 'axios';

class SingleReview extends Component{

    constructor(props){
        super(props);
        this.state={
            item:props.item,
            CoffeeShopName:[]
        }
    }


    componentWillMount(){
        this.getCoffeeShopName();
    }

    getCoffeeShopName(){
        axios.get('http://localhost:3000/api/CoffeeShops/getname?id='+this.state.item.coffeeShopId)
        .then(response => {
            this.setState({CoffeeShopName: response.data},()=>{
                //console.log(this.state.CoffeeShopName.name);
            })
        })
    }
    

    render(){
        return(
            <li className="collection-item">
            <h5>
            {this.state.item.date.split('T')[0]} | {this.state.CoffeeShopName.name}
            </h5>
            {this.state.item.rating} <i className="fa fa-star"></i> - {this.state.item.comments}
            </li>
        )
    }
}

export default SingleReview;