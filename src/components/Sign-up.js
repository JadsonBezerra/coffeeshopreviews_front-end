import React, {Component} from 'react';
import axios from 'axios';


class SignUp extends Component{


    signup(credentials){
        axios.request({
            method:'post',
            url:'http://localhost:3000/api/Reviewers/',
            data:credentials
        }).then(response=>{
            this.props.history.push('/SignIn');
        }).catch(err => console.log(err));
    }

    onSubmit(e){
        const credentials={
            username:this.refs.username.value,
            email:this.refs.email.value,
            password:this.refs.pass.value
        }
        this.signup(credentials);
        e.preventDefault();
        
    }

    render(){
        if(localStorage.getItem("user_id")!=null) this.props.history.push("/");
        return(
            <div>
                <div className="wrapper">
            <div className="container">
            <h4>Sign Up</h4>
                <form onSubmit={this.onSubmit.bind(this)}>
                <div className="input-field">
                    <input type="text" name="username" ref ="username"/>
                    <label htmlFor="Username">Username</label>
                </div>
                <div className="input-field">
                    <input type="text" name="email" ref ="email"/>
                    <label htmlFor="Email">Email</label>
                </div>
                <div className="input-field">
                    <input type="password" name="pass" ref ="pass"/>
                    <label htmlFor="Password">Password</label>
                </div>
                <input type="submit" value="Sign up" className="btn"/>
                </form>
             </div>
             </div>
            </div>
        )
    }
}

export default SignUp;