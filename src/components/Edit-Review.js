import React, {Component} from 'react';
import axios from 'axios';
import Select from '@material-ui/core/Select' ;
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import {Link} from 'react-router-dom';

class EditReview extends Component{
    constructor(props){
        super(props);
        this.state = {
            SelectedCoffeeShop:"",
            Rating:0,
            Comments:"",
            CofeeShopName:"",
            Date:""
        }
        console.log(this.state);
    }
    

    componentWillMount(){
        this.getReview();
    }

    getCoffeeShopName(){
        axios.get('http://localhost:3000/api/CoffeeShops/'+this.state.SelectedCoffeeShop
        +'?access_token='+localStorage.getItem('access_token'))
        .then(response => {this.setState({CofeeShopName:response.data.name},()=>{
            console.log(this.state);
        })
        }).catch(err => console.log(err));
    }


    getReview(){
        axios.get('http://localhost:3000/api/Reviews/'+this.props.match.params.id
        +'?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({SelectedCoffeeShop: response.data.coffeeShopId,
            Rating:response.data.rating,
            Comments:response.data.comments,
            Date:response.data.date},()=>{
                console.log(this.state);
                this.getCoffeeShopName();
            })
        }).catch(err => console.log(err));
        
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value }) 
        console.log(this.state);
    };


    onSubmit(e){
        const Review={
            rating:this.state.Rating,
            comments:this.state.Comments,
            coffeeShopId:this.state.SelectedCoffeeShop,
            publisherId:localStorage.getItem('user_id'),
            id:this.props.match.params.id,
            date:this.state.Date
        }
        console.log(Review);
        axios.request({
            method:'put',
            url:'http://localhost:3000/api/Reviews?access_token='+localStorage.getItem('access_token'),
            data:Review
        }).then(response=>{
            this.props.history.push('/MyReviews');
        }).catch(err => console.log(err));
        e.preventDefault();
    }

    render(){
        if(localStorage.getItem("user_id")==null) this.props.history.push("/SignIn");
        return(
            <div className="container">
                <h3>Edit a Review</h3>
                <br/>
                
                    <h6>Coffee Shop:</h6>
                    <div>
                    <h6>{this.state.CofeeShopName}</h6>
                    </div>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <div className="input-field">
                    <Select
                     value={this.state.Rating}
                     onChange={this.handleChange}
                     name="Rating"
                     >
                        <MenuItem value={0}>
                        0 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={1}>
                        1 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={2}>
                        2 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={3}>
                        3 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={4}>
                        4 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={5}>
                        5 <i className="fa fa-star"/></MenuItem>
                    </Select>
                    </div>
                    <h6>Comments: </h6>
                    <div className="input-field">
                    <TextField
                    fullWidth
                    multiline={true}
                     onChange={this.handleChange}
                     value={this.state.Comments}
                      name="Comments" ref ="Comments"/>
                    </div>
                    <input type="submit" value="Submit Review" className="btn"/>
                    <Link to={`/DeleteReview/${this.props.match.params.id}`}>
                        <button className="btn red right">Delete</button>
                    </Link>
                </form>
            </div>
        )
    }
}

export default EditReview;