import React from 'react';
import { Switch, Route} from 'react-router-dom';
import SignIn from './Sign-in';
import SignUp from './Sign-up';
import All_Reviews from './All-Reviews';
import My_Reviews from './My-reviews';
import LogOut from './Log-out';
import AddReview from './Add-Review';
import EditReview from './Edit-Review';
import DeleteReview from './Delete-Review';
import CoffeeShopReviews from './Coffee-Shops-Reviews';
import CoffeeShops from './Coffee-Shops';

const Main = () =>(
    <main>
        <Switch>
            <Route exact path='/' component={All_Reviews}/>
            <Route exact path='/MyReviews' component={My_Reviews}/>
            <Route exact path='/SignIn' component={SignIn}/>
            <Route exact path='/SignUp' component={SignUp}/>
            <Route exact path='/LogOut' component={LogOut}/>
            <Route exact path='/AddReview' component={AddReview}/>
            <Route exact path='/EditReview/:id' component={EditReview}/>
            <Route exact path='/DeleteReview/:id' component={DeleteReview}/>
            <Route exact path='/CoffeeShopReviews/:id' component={CoffeeShopReviews}/>
            <Route exact path='/CoffeeShops' component={CoffeeShops}/>
        </Switch>
    </main>
)

export default Main;