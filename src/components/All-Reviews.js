import React, {Component} from 'react';
import axios from 'axios';
import SingleReview from './SingleReview';

class All_Reviews extends Component{
    constructor(){
        super();
        this.state = {
            reviews: []
        }

    }


    componentWillMount(){
        this.getReviews();
    }

    getReviews(){
        axios.get('http://localhost:3000/api/Reviews')
        .then(response => {
            this.setState({reviews: response.data},()=>{
                //console.log(this.state);
            })
        })
    }

    render(){
        const reviewsItems = this.state.reviews.map((review,i)=>{
            return(
                <div key={review.id}  className="collection-item">
                <SingleReview item={review}/>
                </div>
            )
        })
        return(
            <div >
                <h4>All Reviews</h4>
                <ul className="collection">
                    {reviewsItems}
                </ul>
            </div>
        )
    }
}

export default All_Reviews;