import React, {Component} from 'react';
import axios from 'axios';
import SingleCofeeShop from './Single-Cofee-Shop';


class CofeeShops extends Component{
    constructor(){
        super();
        this.state = {
            CoffeeShops: []
        }
    }


    componentWillMount(){
        this.getCofeeShops();
    }

    getCofeeShops(){
        axios.get('http://localhost:3000/api/CoffeeShops?access_token='
        +localStorage.getItem('access_token'))
        .then(response => {
            this.setState({CoffeeShops: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }

    render(){
        if(localStorage.getItem("user_id")==null) this.props.history.push("/SignIn");
        const CofeeShopItems = this.state.CoffeeShops.map((CofeeShop,i)=>{
            return(
                    <SingleCofeeShop  key={CofeeShop.id} item={CofeeShop}/>
            )
        })
        return(
            <div>
                <h4>All Coffee Shops</h4>
                <ul className="collection">
                    {CofeeShopItems}
                </ul>
            </div>
        )
    }
}

export default CofeeShops;