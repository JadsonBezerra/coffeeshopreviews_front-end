import React, {Component} from 'react';
import axios from 'axios';



class LogOut extends Component{
  constructor(){
      super();
        axios.request({
          method:'post',
          url:'http://localhost:3000/api/Reviewers/logout?access_token='+localStorage.getItem('access_token')
      }).then(response => {
              localStorage.removeItem("access_token");
              localStorage.removeItem("user_id");
              this.props.history.push('./SignIn');
            }).catch(err => {
                this.props.history.push('./SignIn');
            });
              
  }
    render(){
        return(
            
            <div>
            <h4>Logging out...</h4>
             </div>
        )
    }
}

export default LogOut;

