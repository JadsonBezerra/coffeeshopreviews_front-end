import React, {Component} from 'react';
import axios from 'axios';



class DeleteReview extends Component{
  constructor(props){
      super(props);
        axios.request({method:'delete',
            url:'http://localhost:3000/api/Reviews/'+this.props.match.params.id
        +'?access_token='+localStorage.getItem('access_token')})
        .then(response => {
            console.log(response);
              this.props.history.push('/MyReviews');
            }).catch(err => {
                console.log(err);
                this.props.history.push('/');
            });
              
  }

  
    render(){
        if(localStorage.getItem("user_id")==null) this.props.history.push("/SignIn");
        return(
            
            <div>
            <h4>Deleting Review...</h4>
             </div>
        )
    }
}

export default DeleteReview;

