import React, {Component} from 'react';
import axios from 'axios';



class SignIn extends Component{

    

    login(credentials){
        axios.request({
            method:'post',
            url:'http://localhost:3000/api/Reviewers/login',
            data:credentials
        }).then(response=>{
            console.log(response);
            localStorage.setItem('access_token',response.data.id);
            localStorage.setItem('user_id',response.data.userId);
            this.props.history.push('/MyReviews');
        }).catch(err => console.log(err));
    }

    onSubmit(e){
        const credentials={
            email:this.refs.email.value,
            password:this.refs.pass.value
        }
        this.login(credentials);
        e.preventDefault();
        
    }
  
    render(){
        if(localStorage.getItem("user_id")!=null) this.props.history.push("/");
        return(
            
            <div className="wrapper">
            <div className="container">
            <h4>Sign In <i className="fa fa-user"></i></h4>
                <form onSubmit={this.onSubmit.bind(this)}>
                <div className="input-field">
                    <input type="text" name="email" ref ="email"/>
                    <label htmlFor="Email">Email</label>
                </div>
                <div className="input-field">
                    <input type="password" name="pass" ref ="pass"/>
                    <label htmlFor="Password">Password</label>
                </div>
                <input type="submit" value="Sign in" className="btn"/>
                </form>
             </div>
             </div>
        )
    }
}

export default SignIn;