import React, {Component} from 'react';
import axios from 'axios';
import Select from '@material-ui/core/Select' ;
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

class AddReview extends Component{
    constructor(){
        super();
        this.state = {
            CoffeeShops: [],
            SelectedCoffeeShop:"",
            Rating:0,
            Comments:""
        }
    }
    

    componentWillMount(){
        this.getCoffeeShops();
    }

    getCoffeeShops(){
        axios.get('http://localhost:3000/api/CoffeeShops?access_token='
        +localStorage.getItem('access_token'))
        .then(response => {
            this.setState({CoffeeShops: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value }) 
        console.log(this.state);
    };


    onSubmit(e){
        const Review={
            rating:this.state.Rating,
            comments:this.state.Comments,
            coffeeShopId:this.state.SelectedCoffeeShop,
            publisherId:localStorage.getItem('user_id')
        }
        
        axios.request({
            method:'post',
            url:'http://localhost:3000/api/Reviews?access_token='+localStorage.getItem('access_token'),
            data:Review
        }).then(response=>{
            this.props.history.push('/MyReviews');
        }).catch(err => console.log(err));
        e.preventDefault();
    }

    render(){
        const coffeeshops = this.state.CoffeeShops.map((coffeeshop,i)=>{
            return(

                <MenuItem key={coffeeshop.id} value={coffeeshop.id}>
                {coffeeshop.name}
                </MenuItem>
            )
        })
        return(
            <div className="container">
                <h3>Add a Review</h3>
                <br/>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <h6>Coffee Shop:</h6>
                    <div className="input-field">
                    <Select
                     fullWidth
                     value={this.state.SelectedCoffeeShop}
                     onChange={this.handleChange}
                     name="SelectedCoffeeShop"
                     >
                        {coffeeshops}
                    </Select>
                    </div>
                    <div className="input-field">
                    <Select
                     value={this.state.Rating}
                     onChange={this.handleChange}
                     name="Rating"
                     >
                        <MenuItem value={0}>
                        0 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={1}>
                        1 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={2}>
                        2 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={3}>
                        3 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={4}>
                        4 <i className="fa fa-star"/></MenuItem>
                        <MenuItem value={5}>
                        5 <i className="fa fa-star"/></MenuItem>
                    </Select>
                    </div>
                    <h6>Comments: </h6>
                    <div className="input-field">
                    <TextField
                    fullWidth
                    multiline={true}
                     onChange={this.handleChange}
                     value={this.state.Comments}
                      name="Comments" ref ="Comments"/>
                    </div>
                    <input type="submit" value="Submit Review" className="btn"/>
                </form>
            </div>
        )
    }
}

export default AddReview;