import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};



function ButtonAppBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            CoffeeShop Reviews
            <Icon>thumbs_up_down</Icon>
          </Typography>
          <Link  to="/" style={{color:'#FFF'}}>
          <Button color="inherit">All Reviews</Button>
          </Link>
          <Link hidden={localStorage.getItem("access_token")!=null?null:"hidden"} to="/CoffeeShops" style={{color:'#FFF'}}>
          <Button color="inherit">CoffeeShops</Button>
          </Link>
          <Link hidden={localStorage.getItem("access_token")!=null?null:"hidden"} to="/MyReviews" style={{color:'#FFF'}}>
          <Button color="inherit">My Reviews</Button>
          </Link>
          <Link hidden={localStorage.getItem("access_token")!=null?null:"hidden"} to="/AddReview" style={{color:'#FFF'}}>
          <Button color="inherit">Add Review</Button>
          </Link>
          <Link hidden={localStorage.getItem("access_token")==null?null:"hidden"} to="/SignIn" style={{color:'#FFF'}}>
          <Button color="inherit">Sign In</Button>
          </Link>
          <Link hidden={localStorage.getItem("access_token")==null?null:"hidden"} to="/SignUp" style={{color:'#FFF'}}>
          <Button variant="outlined" color="inherit">Sign Up</Button>
          </Link>
          <Link hidden={localStorage.getItem("access_token")!=null?null:"hidden"} to="/LogOut" style={{color:'#FFF'}}>
          <Button variant="outlined" color="inherit">Log Out</Button>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonAppBar);