import React, {Component} from 'react';
import axios from 'axios';
import SingleReview from './SingleReview';      


class CofeeShopReviews extends Component{
    constructor(props){
        super(props);
        this.state = {
            reviews: [],
            CofeeShopName:""
        }
    }


    componentWillMount(){
        this.getReviews();
    }

    getReviews(){
        axios.get('http://localhost:3000/api/CoffeeShops/'+this.props.match.params.id
        +'/reviews?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({reviews: response.data},()=>{
                console.log(this.state);
            })
        }).catch(err => console.log(err));
    }

    getCoffeeShopName(){
        axios.get('http://localhost:3000/api/CofeeShops/'+this.props.match.params.id
        +'/reviews?access_token='+localStorage.getItem('access_token'))
        .then(response => {
            this.setState({CofeeShopName:response.data.name})
        }).catch(err => console.log(err));
    }

    render(){
        if(localStorage.getItem("user_id")==null) this.props.history.push("/SignIn");
        const reviewsItems = this.state.reviews.map((review,i)=>{
            return(
                <SingleReview key={review.id} item={review}/>
            )
        })
        return(
            <div>
                <h4>My Reviews</h4>
                <ul className="collection">
                    {reviewsItems}
                </ul>
            </div>
        )
    }
}

export default CofeeShopReviews;